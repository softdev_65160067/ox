package com.mycompany.ox;

import java.util.Scanner;

public class Ox {
    public char[][] board;
    public char currentPlayer;
    public int totalMoves;
    public boolean gameOver;

    public Ox() {
        board = new char[3][3];
        currentPlayer = 'X';
        totalMoves = 0;
        gameOver = false;
        initializeBoard();
    }

    private void initializeBoard() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = '-';
            }
        }
    }

    private boolean isValidMove(int move) {
        if (move >= 1 && move <= 9) {
            int row = (move - 1) / 3;
            int col = (move - 1) % 3;
            return board[row][col] == '-';
        }
        return false;
    }

    private boolean isWinner() {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == currentPlayer && board[i][1] == currentPlayer && board[i][2] == currentPlayer) {
                return true; // Check rows
            }
            if (board[0][i] == currentPlayer && board[1][i] == currentPlayer && board[2][i] == currentPlayer) {
                return true; // Check columns
            }
        }
        if (board[0][0] == currentPlayer && board[1][1] == currentPlayer && board[2][2] == currentPlayer) {
            return true; // Check diagonal from top left to bottom right
        }
        else if (board[0][2] == currentPlayer && board[1][1] == currentPlayer && board[2][0] == currentPlayer) {
            return true; // Check diagonal from top right to bottom left
        }
        return false;
    }

    private void printBoard() {
        System.out.println("-------------");
        for (int row = 0; row < 3; row++) {
            System.out.print("| ");
            for (int col = 0; col < 3; col++) {
                System.out.print(board[row][col] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }

    private void resetGame() {
        currentPlayer = 'X';
        totalMoves = 0;
        gameOver = false;
        initializeBoard();
    }

    public void playGame() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome To XOXO GAME!!");

        while (!gameOver) {
            printBoard(); 
            System.out.print("Player " + currentPlayer + ", enter your move (1-9): ");

            int move = 0;
            boolean validInput = false;

            while (!validInput) {
                if (scanner.hasNextInt()) {
                    move = scanner.nextInt();
                    validInput = true;
                } else {
                    System.out.print("Invalid input. Player " + currentPlayer + ", enter your move (1-9): ");
                    scanner.next();
                }
            }

            if (isValidMove(move)) {
                int row = (move - 1) / 3;
                int col = (move - 1) % 3;
                board[row][col] = currentPlayer;

                if (isWinner()) {
                    printBoard();
                    System.out.println("Player " + currentPlayer + " wins!");
                    gameOver = true;

                    System.out.print("Do you want to play again? (Y/N): ");
                    String playAgain = scanner.next();

                    while (!playAgain.equalsIgnoreCase("Y") && !playAgain.equalsIgnoreCase("N")) {
                        System.out.print("Invalid input. Please enter Y or N: ");
                        playAgain = scanner.next();
                    }
                
                    if (playAgain.equalsIgnoreCase("N")) {
                        System.out.println("BYE BYE!!");
                        gameOver = true;
                    } else {
                        resetGame();
                        System.out.println("Welcome To XOXO GAME Again!!");
                    }


                //if (playAgain.equalsIgnoreCase("N")) {
                    //System.out.println("BYE BYE!!");
                    //gameOver = true;
                //} else {
                    //resetGame();
                //}


                } else if (totalMoves == 8) {
                    printBoard();
                    System.out.println("It's a draw!");
                    gameOver = true;

                    System.out.print("Do you want to play again? (Y/N): ");
                    String playAgain = scanner.next();

                    while (!playAgain.equalsIgnoreCase("Y") && !playAgain.equalsIgnoreCase("N")) {
                        System.out.print("Invalid input. Please enter Y or N: ");
                        playAgain = scanner.next();
                    }
                
                    if (playAgain.equalsIgnoreCase("N")) {
                        System.out.println("BYE BYE!!");
                        gameOver = true;
                    } else {
                        resetGame();
                        System.out.println("Welcome To XOXO GAME Again!!");
                    }


                } else {
                    currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
                    totalMoves++;
                }
            } else {
                System.out.println("Invalid move. Please try again.");
            }
            
        }

        scanner.close();
    }

    
    public static void main(String[] args) {
        Ox game = new Ox();
        game.playGame();
    }
}


